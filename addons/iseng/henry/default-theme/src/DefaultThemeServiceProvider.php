<?php namespace Henry\DefaultTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

class DefaultThemeServiceProvider extends AddonServiceProvider
{

    protected $plugins = [];

    protected $commands = [];

    protected $routes = [];

    protected $middleware = [];

    protected $listeners = [];

    protected $aliases = [];

    protected $bindings = [];

    protected $providers = [];

    protected $singletons = [];

    protected $overrides = [
        'streams::errors/404' => 'theme::errors/404',
        'streams::errors/500' => 'theme::errors/500',
    ];

    protected $mobile = [];

    public function register()
    {
    }

    public function map()
    {
    }

}
